package trakkr.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

//Entity -> Representation of data in our table
// File names -> PascalCase

@Entity(name = "User")
@Table(name = "users")

data class User (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null, // BIGINT is Long
    @Column(
        nullable = false,
        updatable = true,
        name = "firstName"
    )
    var firstName: String,
    @Column(
        nullable = false,
        updatable = true,
        name = "lastName"
    )
    var lastName: String,
    @Column(
        nullable = false,
        updatable = false,
        name = "email"
    )
    var email: String,
    @Column(
        nullable = false,
        updatable = true,
        name = "isActive"
    )
    var isActive: Boolean = true,
    @Column(
        nullable = false,
        updatable = true,
        name = "userType"
    )
    var userType: String,

    @JsonIgnoreProperties(value = ["user"], allowSetters = true)
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    val contactDetails: List<Contact> = mutableListOf()
)

//1. Create entity
//2. Create SQL
//3. Create Respository
//4. Create Service
//6. Create handler
// Moving forward, repeat steps 4-6, revise step 1, 2 only if
// there are change in the database structure