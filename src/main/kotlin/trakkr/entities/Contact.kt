package trakkr.entities

import javax.persistence.*


@Entity(name = "Contact")
@Table(name = "contacts")

data class Contact(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    @Column(
        nullable = false,
        updatable = true,
        name = "contactDetails"
    )
    var contactDetails: String,
    @Column(
        nullable = false,
        updatable = true,
        name = "contactType"
    )
    var contactType: String,
    @Column(
        nullable = false,
        updatable = true,
        name = "isPrimary"
    )
    var isPrimary: Boolean = true,

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    var user: User? = null
)