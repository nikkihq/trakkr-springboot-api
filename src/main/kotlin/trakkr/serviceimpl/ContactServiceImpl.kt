package trakkr.serviceimpl

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import trakkr.entities.Contact
import trakkr.repositories.ContactRepository
import trakkr.repositories.UserRepository
import trakkr.service.ContactService

@Service
class ContactServiceImpl(
    private val repository: ContactRepository,
    private val userRepository: UserRepository
): ContactService {

    override fun createContact(body: Contact, userId: Long): Contact {
        val user = userRepository.findById(userId)
            .orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND,
                "Contact with id: $userId does not exist")
        }
        return repository.save(body.copy(
            user = user
        ))
    }

    override fun getContactById(id: Long): Contact {
        val contact = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND,
                "Contact with id: $id does not exist")
        }
        return contact
    }

    override fun updateContactDetails(body: Contact, id: Long): Contact {
        val contact = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND,
                "Contact with id: $id does not exist")
        }
        return repository.save(contact.copy(
            contactDetails = body.contactDetails,
            contactType = body.contactType

        ))
    }

    override fun deleteContact(id: Long){
        val contact = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND,
                "User id: $id does not exist")
        }
//    2. Delete user
        repository.delete(contact)
    }

    override fun getAllContacts(): List<Contact> {
        val contact = repository.findAll()

        return contact.toList()
    }
}