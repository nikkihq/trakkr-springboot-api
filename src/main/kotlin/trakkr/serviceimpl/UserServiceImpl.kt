package trakkr.serviceimpl

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import trakkr.entities.User
import trakkr.repositories.UserRepository
import trakkr.service.UserService

@Service
class UserServiceImpl(
    private val repository: UserRepository
) : UserService {

    // Create a user
    override fun createUser(user: User): User {
        //Check if the email already exist in the Database
       //Create a custom query
        if (repository.doesEmailExist(user.email)){
            throw ResponseStatusException(HttpStatus.CONFLICT,
            "Email ${user.email} already exists!")
        }

        //save the user using the UserRepository
        return repository.save(user)
    }

//  Get a specific user using an id
    override fun getById(id: Long): User {
//    Find the user in the Database and if does not exis
//    it will throw a 404 error
        val user = repository.getUserWithContactDetails(id)
            .orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND,
                "User with id: $id does not exist")
        }

        return user
    }

    override fun updateUser(body: User, id: Long): User {
//      1. Find the entity first
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND,
            "User id: $id does not exist")
        }

//      2. Use save(). If the object does not exist yet in the
//      database, it will insert the object. Else it will update the
//      object
        return repository.save(user.copy(
            firstName = body.firstName,
            lastName = body.lastName,
            email = body.email
        ))
    }

    override fun deleteUser(id: Long) {
//   1. Find the entity
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND,
            "User id: $id does not exist")
        }
//    2. Delete user
        repository.delete(user)
    }

    override fun getAllUsers(): List<User> {

//      findAll returns a collection
        val users = repository.findAll()

        return users.toList()
    }
}