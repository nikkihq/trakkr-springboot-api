package trakkr.service

import trakkr.entities.Contact

interface ContactService {
    fun createContact(body: Contact, userId: Long): Contact
    fun getContactById(id: Long): Contact
    fun updateContactDetails(body: Contact, id: Long): Contact
    fun deleteContact(id: Long)
    fun getAllContacts(): List<Contact>
}