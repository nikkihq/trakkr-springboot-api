package trakkr.service

import trakkr.entities.User

interface UserService {
    fun createUser(body: User): User // done
    fun getById(id: Long): User // done
    fun updateUser(body: User, id: Long): User
    fun deleteUser(id: Long)
    fun getAllUsers(): List<User>
}