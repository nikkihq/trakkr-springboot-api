package trakkr.repositories

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import trakkr.entities.Contact

@Repository
interface ContactRepository: CrudRepository<Contact, Long>