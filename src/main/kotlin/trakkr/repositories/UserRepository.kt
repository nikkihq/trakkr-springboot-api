package trakkr.repositories

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import trakkr.entities.User
import java.util.*

// Extend CrudRepository Interface to easily access the
// basic database operations

@Repository
interface UserRepository: CrudRepository<User, Long>{

//    Do not use the table name, use the entity name
    @Query(
        """
            SELECT CASE WHEN COUNT(u) > 0 THEN TRUE ELSE FALSE END
            FROM User u WHERE u.email = :email
        """
    )
    fun doesEmailExist(email: String): Boolean

    @Query(
        """
            SELECT u FROM User u
            LEFT JOIN FETCH u.contactDetails cd
            WHERE u.id = :id
        """
    )

    fun getUserWithContactDetails(id: Long): Optional<User>
}