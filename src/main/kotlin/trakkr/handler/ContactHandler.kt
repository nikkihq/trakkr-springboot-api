package trakkr.handler

import org.springframework.web.bind.annotation.*
import trakkr.entities.Contact
import trakkr.entities.User
import trakkr.service.ContactService

@RestController
class ContactHandler(
    private val contactService: ContactService
) {
    @PostMapping("/api/contacts-details/{userId}/")
    fun createContact(@RequestBody body: Contact,
                      @PathVariable("userId") userId: Long):
            Contact = contactService.createContact(body, userId)

    @GetMapping("/api/contact/{id}/")
    fun getContactById(@PathVariable("id") id: Long): Contact =
        contactService.getContactById(id)

    @PutMapping("/api/contact/{id}/")
    fun updateContactDetails(@RequestBody body: Contact,
                   @PathVariable("id") id: Long): Contact =
        contactService.updateContactDetails(body, id)

    @DeleteMapping("/api/contact/{id}/")
    fun deleteContact(@PathVariable("id") id: Long) =
        contactService.deleteContact(id)

    @GetMapping("/api/contacts/")
    fun getAllContacts(): List<Contact> =
        contactService.getAllContacts()




}